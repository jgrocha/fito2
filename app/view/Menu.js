Ext.define('Agricultor.view.Menu', {
	extend : 'Ext.tree.Panel',
	alias : 'widget.menuprincipal',
	store : 'Menus',
	rootVisible : false,
	displayField : 'titulo'
}); 