CREATE SCHEMA IF NOT EXISTS historico;

DROP TABLE IF EXISTS historico.grupo CASCADE;

CREATE TABLE historico.grupo (
    id serial PRIMARY KEY,
    idgrupo integer,
    nome character varying(45) NOT NULL,
    operacao char(1), 
    datacriacao timestamp with time zone DEFAULT now(),
    idutilizador integer REFERENCES utilizador (id) -- utilizador
);

CREATE OR REPLACE FUNCTION historico_grupo()
RETURNS TRIGGER AS 
$historico_grupo$
BEGIN
	IF (TG_OP = 'DELETE') THEN
		INSERT INTO historico.grupo (idgrupo, nome, operacao) VALUES( OLD.id, OLD.nome, 'D');
	ELSIF (TG_OP = 'UPDATE') THEN
		IF (NEW.idutilizador IS NOT NULL) THEN
			INSERT INTO historico.grupo (idgrupo, nome, operacao, idutilizador) VALUES( OLD.id, OLD.nome, 'U', NEW.idutilizador);
		ELSE
			INSERT INTO historico.grupo (idgrupo, nome, operacao) VALUES( OLD.id, OLD.nome, 'U');
		END IF;
	END IF;
	RETURN NULL;
END; 
$historico_grupo$ 
LANGUAGE 'plpgsql';

DROP TRIGGER IF EXISTS historico_grupo ON grupo;
CREATE TRIGGER historico_grupo
	AFTER UPDATE OR DELETE ON grupo
		FOR EACH ROW EXECUTE PROCEDURE historico_grupo();