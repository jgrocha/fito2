Ext.define('Agricultor.view.AnexoFormulario', {
	extend : 'Ext.window.Window',
	alias : 'widget.anexoformulario',
	stores : ['TipoAnexos'],
	height : 160,
	width : 500,
	layout : {
		type : 'fit'
	},
	title : 'Anexo',
	items : [{
		xtype : 'form',
		bodyPadding : 5,
		layout : {
			type : 'hbox', // #1
			align : 'stretch'
		},
		items : [{
			xtype : 'fieldset',
			flex : 2,
			title : 'Anexo',
			defaults : {
				anchor : '100%',
				xtype : 'textfield',
				allowBlank : false,
				labelWidth : 60
			},
			items : [{
				xtype : 'hiddenfield',
				name : 'id'
			}, {
				xtype : 'hiddenfield',
				name : 'idrequerimento'
			}, {
				name : 'tipo',
				xtype : 'combobox',
				fieldLabel : 'Escolha o tipo de anexo',
				displayField : 'designacao',
				valueField : 'id',
				width : 420,
				labelWidth : 130,
				store : 'TipoAnexos',
				queryMode : 'local'
			}, {
				xtype : 'filefield',
				emptyText : 'Escolha o documento',
				fieldLabel : 'Documento',
				name : 'documento'
			}]
		}]
	}],
	dockedItems : [{
		xtype : 'toolbar',
		flex : 1,
		dock : 'bottom',
		layout : {
			pack : 'end',
			type : 'hbox'
		},
		items : [{
			xtype : 'button',
			text : 'Cancelar',
			itemId : 'cancela'
		}, {
			xtype : 'button',
			text : 'Adicionar',
			itemId : 'adiciona'
		}]
	}]
});
