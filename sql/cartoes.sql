-- SET client_encoding = 'UTF8';

DROP TABLE IF EXISTS tipoanexo CASCADE;
CREATE TABLE tipoanexo (
    id serial PRIMARY KEY,
    designacao character varying(60) NOT NULL 
);

INSERT INTO tipoanexo (designacao) VALUES ('Procuração');
INSERT INTO tipoanexo (designacao) VALUES ('Documento de identificação do país de origem'); 
INSERT INTO tipoanexo (designacao) VALUES ('Formação de aplicação de produtos fitofarmacêuticos');
INSERT INTO tipoanexo (designacao) VALUES ('Formação agrícola');
INSERT INTO tipoanexo (designacao) VALUES ('Aproveitamento em prova de conhecimentos');
INSERT INTO tipoanexo (designacao) VALUES ('Comprovativo de pagamento');
INSERT INTO tipoanexo (designacao) VALUES ('Outro');

DROP TABLE IF EXISTS estado CASCADE;
CREATE TABLE estado (
    id serial PRIMARY KEY,
    designacao character varying(48) NOT NULL 
);

INSERT INTO estado (designacao) VALUES ('Recebido'); -- Foi submetido
INSERT INTO estado (designacao) VALUES ('Aguarda validação'); -- Foi submetido
INSERT INTO estado (designacao) VALUES ('Validado'); -- Toda a documentação foi validada
INSERT INTO estado (designacao) VALUES ('Incompleto'); -- Toda a documentação foi validada
INSERT INTO estado (designacao) VALUES ('Expirou'); -- Toda a documentação foi validada
INSERT INTO estado (designacao) VALUES ('Pagamento'); -- Em pagamento...
INSERT INTO estado (designacao) VALUES ('Concluído'); -- Dá lugar à emissão do cartão

DROP TABLE IF EXISTS requerimento CASCADE;
CREATE TABLE requerimento (
	id serial PRIMARY KEY,
    idutilizador integer REFERENCES utilizador (id),
    idtitular integer REFERENCES utilizador (id),
    datacriacao timestamp with time zone DEFAULT now(),
    datamodificacao timestamp with time zone DEFAULT now(),    
    estado integer REFERENCES estado (id),
    observacoes text    
);

insert into requerimento (idutilizador, idtitular, estado, observacoes) 
VALUES (2, 1, 1, 'Requerimento de teste' );
insert into requerimento (idutilizador, idtitular, estado, observacoes) 
VALUES (2, 2, 2, 'Requerimento de teste' );

DROP TABLE IF EXISTS anexo CASCADE;
CREATE TABLE anexo (
	id serial PRIMARY KEY,
	idrequerimento integer REFERENCES requerimento (id),
    -- idtitular integer NOT NULL -- titular
	documento character varying(100),
	tipo integer REFERENCES tipoanexo (id),
	tamanho integer,
	-- http://www.verot.net/php_class_upload_download.htm
    datacriacao timestamp with time zone DEFAULT now(),
    datamodificacao timestamp with time zone,    
    -- poderá ter que haver uma tabela própria para validações
    validado boolean DEFAULT NULL, -- 3 valores: NULL, FALSE, TRUE
    idutilizador integer REFERENCES utilizador (id), -- utilizador
    datavalidacao timestamp with time zone DEFAULT NULL
);

INSERT INTO anexo (	idrequerimento, documento, tipo, tamanho, validado, idutilizador) values (2, 'upload/anexos/2/Socrates1.jpg', 4, 1170391, TRUE, 2 );

-- select * from requerimento;
CREATE OR REPLACE VIEW requerimentoview AS
select req.id, t.nome, req.datacriacao, e.designacao, req.observacoes 
from requerimento req, utilizador t, estado e
where req.idtitular = t.id and e.id = req.estado;

-- select * from requerimentoview;