Ext.define('Agricultor.model.Anexo', {
	extend : 'Ext.data.Model',
	fields : [{
		name : 'id',
		type : 'int'
	}, {
		name : 'idrequerimento',
		type : 'int'
	}, {
		name : 'documento',
		type : 'auto'
	}, {
		name : 'tipo',
		type : 'int'
	}, {
		name : 'tamanho',
		type : 'int'
	}, {
		name : 'datacriacao',
		type : 'date'
	}, {
		name : 'validado',
		type : 'boolean'
	}, {
		name : 'datavalidacao',
		type : 'date'
	}]
});