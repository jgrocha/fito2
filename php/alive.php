<?php
session_start();
if (isset($_SESSION['identificado']) && $_SESSION['identificado']) {
	$grupo = $_SESSION['grupo'];
	$login = $_SESSION['utilizador'];
	$tempoonline = time() - $_SESSION['ultima_atividade'];
	$result['success'] = true;
	$result['utilizador']['login'] = $login;	
	$result['utilizador']['online'] = date('H:i:s', $tempoonline);	
	$result['utilizador']['grupo'] = $grupo;	
} else {
	$result['success'] = false;
}
header ( 'Content-type: application/json' );
echo json_encode ( $result );
?>