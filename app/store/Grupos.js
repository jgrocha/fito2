Ext.define('Agricultor.store.Grupos', {
	extend : 'Ext.data.Store',
	model : 'Agricultor.model.Grupo',
	autoLoad: false, // após o render da grid
	autoSync : true,
	pageSize : 35,
	proxy : {
		type : 'ajax',
		extraParams : {
			tabela : 'grupo',
			root : 'grupos'
		},
		api : {
			create : 'php/cria.php',
			read : 'php/lista.php',
			update : 'php/atualiza.php',
			destroy : 'php/remove.php'
		},
		reader : {
			type : 'json',
			root : 'grupos',
			successProperty : 'success'
		},
		writer : {
			type : 'json',
			writeAllFields : false,
			encode : true,
			root : 'grupos'
		}
	}
});