Ext.define('Agricultor.model.Menu', {
	extend : 'Ext.data.Model',

	fields : [{
		name : 'id',
		type : 'int'
	}, {
		name : 'titulo',
		type : 'string'
	}, {
		name : 'class',
		type : 'string'
	}]

}); 