Ext.define('Agricultor.store.Requerimentos', {
	extend : 'Ext.data.Store',
	model : 'Agricultor.model.Requerimento',
	autoLoad: false, // após o render da grid
	autoSync : true,
	pageSize : 35,
	proxy : {
		type : 'ajax',
		extraParams : {
			tabela : 'requerimentoview',
			root : 'requerimentos'
		},
		api : {
			read : 'php/lista.php'
		},
		reader : {
			type : 'json',
			root : 'requerimentos',
			successProperty : 'success'
		}
	}
});