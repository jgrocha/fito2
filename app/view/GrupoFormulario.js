Ext.define('Agricultor.view.GrupoFormulario', {
	extend : 'Ext.window.Window',
	alias : 'widget.grupoformulario',
	height : 160,
	width : 240,
	layout : {
		type : 'fit'
	},
	title : 'Grupo',
	items : [ {
		xtype : 'form',
		bodyPadding : 5,
		layout : {
			type : 'hbox', // #1
			align : 'stretch'
		},
		items : [ {
			xtype : 'fieldset',
			flex : 2,
			title : 'Dados do Grupo',
			defaults : {
				anchor : '100%',
				xtype : 'textfield',
				allowBlank : false,
				labelWidth : 60
			},
			items : [ {
				xtype : 'hiddenfield',
				fieldLabel : 'Label',
				name : 'id'
			}, {
				fieldLabel : 'Nome',
				maxLength : 100,
				name : 'nome'
			} ]
		} ]
	} ],
	dockedItems : [ {
		xtype : 'toolbar',
		flex : 1,
		dock : 'bottom',
		layout : {
			pack : 'end',
			type : 'hbox'
		},
		items : [ {
			xtype : 'button',
			text : 'Cancel',
			action : 'cancela'
		}, {
			xtype : 'button',
			text : 'Gravar',
			itemId : 'grava'
		} ]
	} ]
});