# Agricultor/app

Esta é a pasta mais importante, onde está a aplicação estruturada segundo o padrão MVC, com as seguintes subpastas.

## Agricultor/app/controller
## Agricultor/app/model
## Agricultor/app/store
## Agricultor/app/view

# Agricultor/resources

This folder contains static resources (typically an `"images"` folder as well).

# Agricultor/overrides

This folder contains override classes. All overrides in this folder will be 
automatically included in application builds if the target class of the override
is loaded.

# Agricultor/sass/etc

This folder contains misc. support code for sass builds (global functions, 
mixins, etc.)

# Agricultor/sass/src

This folder contains sass files defining css rules corresponding to classes
included in the application's javascript code build.  By default, files in this 
folder are mapped to the application's root namespace, 'Agricultor'. The
namespace to which files in this directory are matched is controlled by the
app.sass.namespace property in Agricultor/.sencha/app/sencha.cfg. 

# Agricultor/sass/var

This folder contains sass files defining sass variables corresponding to classes
included in the application's javascript code build.  By default, files in this 
folder are mapped to the application's root namespace, 'Agricultor'. The
namespace to which files in this directory are matched is controlled by the
app.sass.namespace property in Agricultor/.sencha/app/sencha.cfg. 

# Publicar a aplicação

Acrescentar regra para copiar o código PHP para a pasta de produção, em build.xml.

* Deploy com PhoneGap, http://www.sencha.com/blog/getting-started-with-sencha-touch-2-build-a-weather-utility-app-part-3/
* Enviar emails com mais flexibilidade, http://email.about.com/od/emailprogrammingtips/qt/PHP_Email_SMTP_Authentication.htm
* Melhorar a autenticação/sessões PHP com o módulo Pear::Auth, http://pear.php.net/manual/en/package.authentication.auth.php

# Build

	<!-- <link rel="stylesheet" href="bootstrap.css"> -->
	<link rel="stylesheet" type="text/css" href="ext/packages/ext-theme-classic/build/resources/ext-theme-classic-all.css">
