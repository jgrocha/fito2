/*
 *
 */
Ext.define('Agricultor.controller.Cabecalho', {
	extend : 'Ext.app.Controller',
	// Ext.ComponentQuery.query('cabecalho button#botaoLogin')
	refs : [{
		selector : 'cabecalho button#botaoLogin',
		ref : 'botaoLogin' // gera um getBotaoLogin
	}, {
		selector : 'cabecalho button#botaoLogout',
		ref : 'botaoLogout' // gera um getBotaoLogout
	}, {
		selector : 'cabecalho button#botaoRegisto',
		ref : 'botaoRegisto' // gera um getBotaoRegisto
	}, {
		selector : 'cabecalho splitbutton',
		ref : 'botaoMenu' // gera um getBotaoMenu
	}],
	init : function() {
		console.log('O controlador está a arrancar...');
		this.control({
			"cabecalho button#botaoRegisto" : {
				click : this.onButtonClickRegisto
			},
			"cabecalho button#botaoLogin" : {
				click : this.onButtonClickLogin
			},
			"cabecalho splitbutton #botaoPerfil" : {
				click : this.onButtonClickPerfil
			},
			"cabecalho splitbutton #botaoLogout" : {
				click : this.onButtonClickLogout
			},
			"cabecalho splitbutton" : {
				click : this.onButtonMenu
			},
			"login form button#entrar" : {
				click : this.onButtonClickEntrar
			},
			"login form button#cancelar" : {
				click : this.onButtonClickCancelar
			},
			"login form textfield" : {
				// é disparado tanto em field.name = "user" como em field.name = "password"
				specialkey : this.onTextfieldSpecialKey
			}
		});
		this.application.on({
			scope : this,
			loginComSucesso : this.onLogin,
			logoutComSucesso : this.onLogout
		});
	},
	onLaunch : function() {
		console.log('...O controlador arrancou.');
		this.getBotaoMenu().menu.disable();
	},
	onLogout : function() {
		console.log('Vamos reagir ao evento logoutComSucesso');
		// ativar botão de registo
		this.getBotaoRegisto().setDisabled(false);
		this.getBotaoLogin().setText('Iniciar sessão');
		this.getBotaoMenu().menu.disable();
		Ext.state.Manager.clear("utilizador");
	},
	onLogin : function() {
		console.log('Vamos reagir ao evento loginComSucesso');
		// desativar botão de registo
		this.getBotaoRegisto().setDisabled(true);
		Agricultor.user = Ext.state.Manager.get("utilizador");
		this.getBotaoLogin().setText(Agricultor.user.nome);
		this.getBotaoMenu().menu.enable();
		// Mostra o menu, só por curiosidade...
		// this.getBotaoMenu().showMenu();
	},
	onButtonClickRegisto : function(button, e, options) {
		Ext.Msg.show({
			title : 'Registo de utilizadores',
			msg : 'O registo de novos utilizadores está temporariamente indisponível.',
			icon : Ext.Msg.INFO,
			buttons : Ext.Msg.OK
		});
	},
	onButtonMenu : function(button, e, options) {
		console.log('onButtonMenu');
	},
	onButtonClickPerfil : function(button, e, options) {
		console.log('Vamos mostrar e permitir atualizar o perfil do utilizador');
	},
	onButtonClickLogin : function(button, e, options) {
		console.log('Vamos autenticar um utilizador, se não estiver já loginado');
		var user = Ext.state.Manager.get("utilizador");
		if (user == null) {
			var win = Ext.create('Agricultor.view.Login');
			win.show();
		} else {
			console.log('Afinal user != null');
			console.debug(user);
		}
	},
	onButtonClickEntrar : function(button, e, options) {
		console.log('login submit');
		var formPanel = button.up('form'), login = button.up('login'), user = formPanel.down('textfield[name=user]').getValue(), pass = formPanel.down('textfield[name=password]').getValue();
		var sha1 = CryptoJS.SHA1(pass).toString();
		if (formPanel.getForm().isValid()) {
			Ext.get(login.getEl()).mask("A validar a identificação... Aguarde...", 'loading');
			Ext.Ajax.request({
				scope : this,
				url : 'php/login.php',
				params : {
					user : user,
					password : sha1
				},
				success : function(conn, response, options, eOpts) {
					console.debug(conn);
					Ext.get(login.getEl()).unmask();
					var resultado = Ext.JSON.decode(conn.responseText);
					if (resultado.success) {
						login.close();
						Ext.state.Manager.set('utilizador', {
							login : resultado.utilizador.login,
							nome : resultado.utilizador.nome
						});
						this.application.fireEvent('loginComSucesso');
					} else {
						Ext.Msg.show({
							title : 'Erro na identificação do utilizador',
							msg : resultado.errors.reason, // conn.responseText,
							icon : Ext.Msg.ERROR,
							buttons : Ext.Msg.OK
						});
					}
				},
				failure : function(conn, response, options, eOpts) {
					Ext.get(login.getEl()).unmask();
					Ext.Msg.show({
						title : 'Erro na ligação',
						msg : conn.responseText,
						icon : Ext.Msg.ERROR,
						buttons : Ext.Msg.OK
					});
				}
			});
		}
	},
	onButtonClickCancelar : function(button, e, options) {
		console.log('login cancel');
		button.up('form').getForm().reset();
		var login = button.up('login');
		login.close();
	},
	onButtonClickLogout : function(button, e, options) {
		console.log('logout!');
		Ext.Ajax.request({
			scope : this,
			url : 'php/logout.php',
			success : function(conn, response, options, eOpts) {
				var resultado = Ext.JSON.decode(conn.responseText);
				if (resultado.success) {
					console.log('Successo!');
					this.application.fireEvent('logoutComSucesso');
				} else {
					console.log('Insucesso...');
				}
			},
			failure : function(conn, response, options, eOpts) {
				console.log('Failure...');
			}
		});
	},
	onTextfieldSpecialKey : function(field, e, options) {
		if (e.getKey() == e.ENTER) {
			console.log('Carregou no ENTER');
			var entrar = field.up('form').down('button#entrar');
			entrar.fireEvent('click', entrar, e, options);
		}
	}
});
