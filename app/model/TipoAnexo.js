Ext.define('Agricultor.model.TipoAnexo', {
	extend : 'Ext.data.Model',
	fields : [{
		name : 'id',
		type : 'int'
	}, {
		name : 'designacao',
		type : 'auto'
	}]
});
