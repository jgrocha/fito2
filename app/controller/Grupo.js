Ext.define('Agricultor.controller.Grupo', {
	extend : 'Ext.app.Controller',
	models : ['Grupo'],
	stores : ['Grupos'],
	requires : ['Ext.window.MessageBox'],
	// Referencing view instances
	refs : [{
		// A component query
		selector : 'viewport > app-main > tabpanel > grupo',
		ref : 'tabelaGrupos' // gera um getTabelaGrupos
	}],
	init : function() {
		console.log('O controlador foi inicializado');
		// escutar eventos da UI
		this.control({
			// observar a vista onde são apresentados os grupos
			'grupo' : {
				selectionchange : this.onGrupoSelect,
				render : this.render
			},
			"grupo button#criagrupo" : {
				click : this.onButtonClickFormCriaGrupo
			},
			"grupo button#modificagrupo" : {
				click : this.onButtonClickFormModificaGrupo
			},
			"grupo button#apagagrupo" : {
				click : this.onButtonClickApagaGrupo
			},
			// observar a janela com o formulário onde são criados/modificados
			// os grupos
			"grupoformulario button[action=cria]" : {
				click : this.onButtonClickCriaGrupo
			},
			"grupoformulario button[action=modifica]" : {
				click : this.onButtonClickModificaGrupo
			},
			"grupoformulario button[action=cancela]" : {
				click : this.onButtonClickCancelaGravaGrupo
			}
		});
		this.getGruposStore().proxy.addListener("exception", function(proxy, response, operation) {
			var resultado = Ext.JSON.decode(response.responseText);
			console.debug(response);
			console.debug(operation);
			Ext.Msg.show({
				title : 'Erro',
				msg : resultado.errors.reason,
				icon : Ext.Msg.ERROR,
				buttons : Ext.Msg.OK
			});
			this.getGruposStore().rejectChanges();
		}, this);
	},
	onButtonClickFormCriaGrupo : function(button, e, options) {
		console.log('Vamos criar um grupo');
		var win = Ext.create('Agricultor.view.GrupoFormulario');
		console.debug(win.down('button#grava'));
		win.down('button#grava').action = 'cria';
		win.setTitle('Criar um novo grupo');
		win.show();
		console.debug(button);
	},
	onButtonClickFormModificaGrupo : function(button, e, options) {
		console.log('Vamos modificar um grupo');
		var grid = this.getTabelaGrupos();
		// devivado do refs [ ]
		record = grid.getSelectionModel().getSelection();
		if (record[0]) {
			var editWindow = Ext.create('Agricultor.view.GrupoFormulario');
			editWindow.down('form').loadRecord(record[0]);
			editWindow.setTitle(record[0].get('nome'));
			console.debug(editWindow.down('button#grava'));
			editWindow.down('button#grava').action = 'modifica';
			editWindow.show();
		}
	},
	onButtonClickApagaGrupo : function(button, e, options) {
		var grid = this.getTabelaGrupos();
		var store = grid.getStore();
		record = grid.getSelectionModel().getSelection();
		console.debug(record[0]);
		if (record[0]) {
			Ext.Msg.show({
				title : 'Apagar grupo',
				msg : 'Tem a certeza que quer remover o grupo ' + record[0].data.nome + '?',
				buttons : Ext.Msg.YESNO,
				icon : Ext.Msg.QUESTION,
				fn : function(buttonId) {
					if (buttonId == 'yes') {
						store.remove(record[0]);
					}
				}
			});
		} else {
			console.log('Está um grupo selecionado?');
		}
	},
	onButtonClickCriaGrupo : function(button, e, options) {
		console.log('Vamos criar o novo grupo');
		var win = button.up('window'), form = win.down('form'), values = form.getValues();
		this.getGruposStore().add(values);
		win.close();
	},
	onButtonClickModificaGrupo : function(button, e, options) {
		console.log('Vamos gravar as alterações');
		var win = button.up('window'), form = win.down('form'), record = form.getRecord(), values = form.getValues();
		record.set(values);
		win.close();
	},
	onButtonClickCancelaGravaGrupo : function(button, e, options) {
		console.log('Cancela grava grupo');
		var win = button.up('window');
		win.close();
	},
	render : function(component, options) {
		console.log('a grid grupo foi renderizada.');
		// component.getStore().load();
		component.getStore().load({
			callback : this.onTipoAnexosLoad,
			scope : this
		});
	},
	onLaunch : function() {
		console.log('O controlador foi lanchado');
	},
	onGruposLoad : function() {
		console.log('O store Grupos foi carregado');
	},
	onGrupoSelect : function(selModel, selection) {
		console.log('O grupo foi selecionado:');
		if (selection[0]) {
			console.debug(selection[0].data);
		}
	}
}); 