Ext.define('Agricultor.controller.Requerimento', {
	extend : 'Ext.app.Controller',
	models : ['Requerimento'],
	stores : ['Requerimentos', 'Anexos', 'TipoAnexos'],
	requires : ['Ext.window.MessageBox', 'Agricultor.view.RequerimentoFormulario', 'Agricultor.view.AnexoFormulario'],
	// Referencing view instances
	refs : [{
		selector : 'viewport > app-main > tabpanel > requerimento',
		ref : 'tabela' // gera um getTabela
	}, {
		// Testar na consola:
		// Ext.ComponentQuery.query('requerimentoFormulario > tabpanel > anexos')
		selector : 'requerimentoFormulario > form > tabpanel > anexos',
		ref : 'gridAnexos' // gera um getGridAnexos
	}, {
		selector : 'requerimentoFormulario > tabpanel > panel > form#requerenteForm',
		ref : 'requerenteForm' // gera um getRequerenteForm
	}, {
		selector : 'requerimentoFormulario > tabpanel > panel > form#titularForm',
		ref : 'titularForm' // gera um getTitularForm
	}, {
		selector : 'requerimentoFormulario > tabpanel > panel > form#complementoForm',
		ref : 'complementoForm' // gera um getComplementoForm
	}],
	init : function() {
		console.log('O controlador foi inicializado');
		// escutar eventos da UI
		this.control({
			// observar a vista onde são apresentados os Requerimentos
			'requerimento' : {
				selectionchange : this.onRequerimentoSelect,
				render : this.render
			},
			"requerimento button#cria" : {
				click : this.onButtonClickFormCriaRequerimento
			},
			"requerimento button#modifica" : {
				click : this.onButtonClickFormModificaRequerimento
			},
			"requerimento button#apaga" : {
				click : this.onButtonClickApagaRequerimento
			},
			"requerimentoFormulario form fieldset[checkboxName=cambiaTitular]" : {
				collapse : this.onFieldsetCollapse,
				expand : this.onFieldsetExpand
			},
			"requerimentoFormulario button#cancela" : {
				click : this.onButtonClickCancelaFormulario
			},
			"requerimentoFormulario button#grava" : {
				click : this.onButtonClickGravaFormulario
			},
			"requerimentoFormulario button#submete" : {
				click : this.onButtonClickSubmeteFormulario
			},
			"requerimentoFormulario anexos button#cria" : {
				click : this.onButtonClickAdicionaAnexo
			},
			"requerimentoFormulario anexos button#apaga" : {
				click : this.onButtonClickRemoveAnexo
			},
			"requerimentoFormulario anexos" : {
				selectionchange : this.onRequerimentoFormularioAnexosSelect,
				render : this.renderRequerimentoFormularioAnexos,
				itemdblclick : this.onItemdblclickAnexos
			},
			"anexoformulario button#cancela" : {
				click : this.onButtonClickAnexoFormularioCancela
			},
			"anexoformulario button#adiciona" : {
				click : this.onButtonClickAnexoFormularioAdiciona
			}
		});
		this.getRequerimentosStore().proxy.addListener("exception", function(proxy, response, operation) {
			var resultado = Ext.JSON.decode(response.responseText);
			Ext.Msg.show({
				title : 'Erro',
				msg : resultado.errors.reason,
				icon : Ext.Msg.ERROR,
				buttons : Ext.Msg.OK
			});
			this.getRequerimentosStore().rejectChanges();
		}, this);
	},

	onItemdblclickAnexos : function(grid, record, item, index, e, eOpts) {
		console.log('onItemdblclickAnexos');
		console.debug(record.data.documento);
		window.open(record.data.documento);
	},
	onFieldsetCollapse : function(fs, eOpts) {
		console.log('Controller collapse');
	},
	onFieldsetExpand : function(fs, eOpts) {
		console.log('Controller Expand');
	},
	onButtonClickAnexoFormularioCancela : function(button, e, options) {
		var win = button.up('window');
		win.close();
	},
	onButtonClickAnexoFormularioAdiciona : function(button, e, options) {
		console.log('onButtonClickAnexoFormularioAdiciona');
		var win = button.up('window'), form = win.down('form'), values = form.getValues();
		/*
		* fazer:
		* this.getAnexosStore().add(values);
		* não funciona. Com este add perde-se a informação do(s) filefields, e não chega ao proxy (php) o $_FILES preenchido.
		* Temos mesmo que tratar este upload com uma submissão direta
		*/
		// var store = this.getGridAnexos().getStore();
		var store = this.getAnexosStore();
		if (form.getForm().isValid()) {
			form.getForm().submit({
				params : {
					idrequerimento : 1
				},
				url : 'php/criaAnexo.php',
				success : function(form, action) {
					console.log('success' + ' ' + action.result.success);
					console.debug(action);
					var result = action.result;
					if (result.success) {
						store.load();
						win.close();
					} else {
						Ext.Msg.alert('Erro reportado pelo servidor', result.errors.reason);
					}
				},
				failure : function(form, action) {
					var result = action.result;
					switch (action.failureType) {
						case Ext.form.action.Action.CLIENT_INVALID:
							Ext.Msg.alert('Erro', 'Problemas no preenchimento no formulário');
							break;
						case Ext.form.action.Action.CONNECT_FAILURE:
							Ext.Msg.alert('Erro', 'Erro de comunicação com o servidor');
							break;
						case Ext.form.action.Action.SERVER_INVALID:
							Ext.Msg.alert('Erro', result.errors.reason);
						// action.response.responseText);
					}
				}
			});
		}
	},
	onButtonClickAdicionaAnexo : function(button, e, options) {
		console.log('onButtonClickAdicionaAnexo');
		var win = Ext.create('Agricultor.view.AnexoFormulario');
		win.setTitle('Adicionar anexo');
		win.show();
	},
	onButtonClickRemoveAnexo : function(button, e, options) {
		console.log('onButtonClickRemoveAnexo: falta remover o documento propriamente dito!');
		var grid = this.getGridAnexos();
		console.debug(grid);
		var store = grid.getStore();
		record = grid.getSelectionModel().getSelection();
		console.debug(record[0]);
		if (record[0]) {
			Ext.Msg.show({
				title : 'Apagar Anexo',
				msg : 'Tem a certeza que quer remover o anexo ' + record[0].data.documento + '?',
				buttons : Ext.Msg.YESNO,
				icon : Ext.Msg.QUESTION,
				fn : function(buttonId) {
					if (buttonId == 'yes') {
						store.remove(record[0]);
					}
				}
			});
		} else {
			console.log('De certeza que está um anexo selecionado?');
		}
	},
	onButtonClickCancelaFormulario : function(button, e, options) {
		var win = button.up('window');
		win.close();
	},
	onButtonClickGravaFormulario : function(button, e, options) {
		console.log('Vamos gravar a parte do requerimento');
		console.debug(this.getRequerenteForm().getForm().getFieldValues());
		console.debug(this.getTitularForm().getForm().getFieldValues());
		console.debug(this.getComplementoForm().getForm().getFieldValues());
	},
	onButtonClickSubmeteFormulario : function(button, e, options) {
		console.log('Vamos submeter o requerimento e os anexos');
	},
	onButtonClickFormCriaRequerimento : function(button, e, options) {
		console.log('Vamos criar um Requerimento');
		var win = Ext.create('Agricultor.view.RequerimentoFormulario');
		win.setTitle('Novo requerimento');
		win.show();
	},
	onButtonClickFormModificaRequerimento : function(button, e, options) {
		console.log('Vamos modificar um Requerimento');
		var grid = this.getTabela();
		record = grid.getSelectionModel().getSelection();
		if (record[0]) {
			var editWindow = Ext.create('Agricultor.view.RequerimentoFormulario');
			editWindow.setTitle(record[0].get('nome'));
			editWindow.show();
			// editWindow.down('form').loadRecord(record[0]);
			editWindow.show();
		}
	},
	onButtonClickApagaRequerimento : function(button, e, options) {
		console.log('Vamos apagar um Requerimento');
	},
	render : function(component, options) {
		console.log('a grid Requerimento foi renderizada');
		// component.getStore().load();
		component.getStore().load({
			callback : this.onRequerimentosLoad,
			scope : this
		});
	},
	renderRequerimentoFormularioAnexos : function(component, options) {
		console.log('a grid Anexo foi renderizada');
		component.getStore().load({
			callback : this.onAnexosLoad,
			scope : this
		});
		var store = this.getTipoAnexosStore();
		if (store.getCount() == 0) {
			store.load();
		}
	},
	onLaunch : function() {
		console.log('evento: onLaunch, class: Agricultor.controller.Requerimento');
	},
	onRequerimentosLoad : function() {
		console.log('O store Requerimentos foi carregado');
	},
	onAnexosLoad : function() {
		console.log('O store Anexos foi carregado');
	},
	onRequerimentoSelect : function(selModel, selection) {
		console.log('O Requerimento foi selecionado:');
		if (selection[0]) {
			console.debug(selection[0].data);
		}
	},
	onRequerimentoFormularioAnexosSelect : function(selModel, selection) {
		console.log('O Anexo foi selecionado:');
		/*
		 if (selection[0]) {
		 console.debug(selection[0].data);
		 }
		 */
	}
});
