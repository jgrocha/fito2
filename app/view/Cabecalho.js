Ext.define('Agricultor.view.Cabecalho', {
	extend : 'Ext.toolbar.Toolbar', // estende a classe Toolbar
	alias : 'widget.cabecalho', // podemos usar este nome para criar instâncias
	height : 30,
	items : [{
		xtype : 'label',
		html : 'Aplicação terrestre de produtos fitofarmacêuticos'
	}, {
		xtype : 'tbfill'
	}, {
		xtype : 'button',
		text : 'Novo utilizador',
		itemId : 'botaoRegisto'
	}, /*{
		xtype : 'button',
		text : 'Iniciar sessão',
		itemId : 'botaoLogin'
	}, */ {
		xtype : 'splitbutton',
		text : 'Iniciar sessão',
		itemId : 'botaoLogin',
		menu : [{
			text : 'Últimos acessos'
		}, {
			text : 'Mensagens'
		}, {
			text : 'Perfil',
			itemId : 'botaoPerfil'
		}, {
			text : 'Terminar sessão',
			itemId : 'botaoLogout'
		}]
	}]
}); 