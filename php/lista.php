<?php
// chama o arquivo de conexão com o bd
include ("db.php");
$root = $_REQUEST ['root'];
$tabela = $_REQUEST ['tabela'];

// curl --data 'root=grupos&tabela=grupo' http://localhost/git/fito/php/lista.php

if (isset ( $_REQUEST ['start'] ) && strlen ( trim ( $_REQUEST ['start'] ) ) > 0) {
	$start = $_REQUEST ['start'];
} else {
	$start = 0;
}
if (isset ( $_REQUEST ['limit'] ) && strlen ( trim ( $_REQUEST ['limit'] ) ) > 0) {
	$limit = $_REQUEST ['limit'];
} else {
	$limit = 25;
}
if ($limit > 50) {
	$limit = 50;
}
$queryConta = "select count(*) from " . $tabela;
$res = &$mdb2->query ( $queryConta );
if (PEAR::isError ( $res )) {
	$result ["success"] = false;
	$result ["errors"] ["reason"] = $res->getMessage ();
	$result ["errors"] ["query"] = $queryConta;
} else {
	$row = $res->fetchRow ( MDB2_FETCHMODE_ASSOC );
	$total = $row ['count'];
	
	$sql = "select * from " . $tabela;
	$sql .= " LIMIT " . $limit . " OFFSET " . $start;
	$res = &$mdb2->query ( $sql );
	if (PEAR::isError ( $res )) {
		$result ["success"] = false;
		$result ["errors"] ["reason"] = $res->getMessage ();
		$result ["errors"] ["query"] = $sql;
	} else {
		$tabela = array ();
		while ( $row = $res->fetchRow ( MDB2_FETCHMODE_ASSOC ) ) {
			array_push ( $tabela, $row );
		}
		$result [$root] = $tabela;
		$result ["sql"] = $sql;
		$result ["success"] = true;
		$result ["total"] = $total;
	}
}
header ( 'Content-type: application/json' );
echo json_encode ( $result );
$mdb2->disconnect ();
?>