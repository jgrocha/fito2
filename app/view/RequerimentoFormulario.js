Ext.define('Agricultor.view.Anexos', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.anexos',
	title : 'Anexos ao requerimento',
	store : 'Anexos',
	columns : [{
		header : 'Id',
		dataIndex : 'id',
		width : 20
	}, {
		header : 'Requerimento',
		dataIndex : 'idrequerimento'
	}, {
		header : 'Documento',
		dataIndex : 'documento',
		flex : 1
	}, {
		header : 'Tipo',
		dataIndex : 'tipo'
	}, {
		header : 'Data',
		dataIndex : 'datacriacao'
	}, {
		header : 'Validado',
		dataIndex : 'validado'
	}, {
		header : 'Validado em',
		dataIndex : 'datavalidacao'
	}],
	dockedItems : [{
		xtype : 'toolbar',
		flex : 1,
		dock : 'top',
		items : [{
			xtype : 'button',
			text : 'Adicionar anexo',
			itemId : 'cria'
		}, {
			xtype : 'button',
			text : 'Apagar anexo',
			itemId : 'apaga'
		}]
	}]
});

Ext.define('Agricultor.view.Titular', {
	extend : 'Ext.form.FieldSet',
	alias : 'widget.titular',
	defaults : {
		anchor : '100%',
		xtype : 'textfield',
		allowBlank : false,
		labelWidth : 70
	},
	items : [{
		fieldLabel : 'Nome',
		name : 'nome'
	}, {
		fieldLabel : 'Morada',
		name : 'morada'
	}, {
		fieldLabel : 'Localidade',
		name : 'localidade'
	}, {
		xtype : 'container',
		layout : 'hbox',
		margin : '0 0 5 0',
		defaults : {
			anchor : '100%',
			xtype : 'textfield',
			allowBlank : false,
			labelWidth : 70
		},
		items : [{
			fieldLabel : 'Cod. Postal',
			name : 'codpostal',
			maxLength : 8
			// width: 70
		}, {
			flex : 1,
			labelWidth : 120,
			fieldLabel : 'Designação Postal',
			name : 'despostal'
		}]
	}]
});

Ext.define('Agricultor.view.Complemento', {
	extend : 'Ext.form.Panel',
	alias : 'widget.complemento',
	items : [{
		xtype : 'tabpanel',
		plain : true,
		activeTab : 0,
		defaults : {
			bodyPadding : 10
		},
		items : [{
			title : 'Documentos de Identificação',
			defaults : {
				width : 300,
				labelWidth : 180
			},
			defaultType : 'textfield',
			items : [{
				xtype : 'radiogroup',
				anchor : 'none',
				layout : {
					autoFlex : false
				},
				defaults : {
					margin : '0 15 0 0'
				},
				items : [{
					inputValue : 'particular',
					boxLabel : 'Particular',
					checked : true,
					name : 'pessoacoletiva'
				}, {
					inputValue : 'empresa',
					boxLabel : 'Pessoa Coletiva',
					name : 'pessoacoletiva'
				}]
			}, {
				fieldLabel : 'Número de Identificação Fiscal',
				name : 'nif',
				allowBlank : false
			}]
		}, {
			title : 'Contactos',
			defaults : {
				width : 230
			},
			defaultType : 'textfield',

			items : [{
				fieldLabel : 'Telefone',
				name : 'telefone'
			}, {
				fieldLabel : 'Telemóvel',
				name : 'telemovel'
			}, {
				fieldLabel : 'Email',
				name : 'email',
				vtype : 'email'
			}]
		}, {
			cls : 'x-plain',
			title : 'Informação complementar',
			layout : 'fit',
			margin : '0 0 5 0',
			items : {
				xtype : 'htmleditor',
				name : 'observacoes',
				fieldLabel : 'Observações',
				hideLabel : true
				// value : 'Acrescente mais alguma informação que julgue necessária'
			}
		}]
	}]
});

Ext.define('Agricultor.view.RequerimentoFormulario', {
	extend : 'Ext.window.Window',
	requires : ['Ext.form.Panel', 'Ext.form.FieldSet', 'Ext.form.field.HtmlEditor'],
	alias : 'widget.requerimentoFormulario',
	height : 600,
	width : 600,
	layout : {
		type : 'fit'
	},
	bodyPadding : '10, 10, 10, 10',
	title : 'Requerimento',
	items : [{
		xtype : 'tabpanel',
		plain : true,
		activeTab : 0,
		defaults : {
			bodyPadding : 10
		},
		items : [{
			title : 'Titular',
			items : [{
				xtype : 'form',
				itemId : 'requerenteForm',
				items : [{
					xtype : 'titular',
					itemId : 'requerente',
					title : 'Requerente'
				}]
			}, {
				xtype : 'form',
				itemId : 'titularForm',
				items : [{
					xtype : 'titular',
					checkboxToggle : true,
					checkboxName : 'cambiaTitular',
					collapsed : true,
					itemId : 'cambia',
					title : 'Titular diferente do requerente',
				}]
			}, {
				xtype : 'complemento',
				itemId : 'complementoForm',
			}]
		}, {
			title : 'Anexos',
			disabled : false, // true
			bodyPadding : '0, 0, 0, 0',
			xtype : 'anexos'
		}]
	}],
	dockedItems : [{
		xtype : 'toolbar',
		flex : 1,
		dock : 'bottom',
		layout : {
			pack : 'end',
			type : 'hbox'
		},
		items : [{
			xtype : 'button',
			text : 'Cancelar',
			itemId : 'cancela'
		}, {
			xtype : 'button',
			text : 'Gravar e anexar documentos',
			itemId : 'grava'
		}, {
			xtype : 'button',
			text : 'Submete',
			itemId : 'submete',
			disabled : true
		}]
	}]
});
