Ext.define('Agricultor.view.TipoAnexo', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.tipoanexo',
	title : 'Tipos de Anexos aos requerimentos',
	store : 'TipoAnexos',
	columns : [{
		header : 'Id',
		dataIndex : 'id',
		width : 20
	}, {
		header : 'Designação',
		dataIndex : 'designacao',
		flex : 1
	}],
	dockedItems : [{
		xtype : 'toolbar',
		flex : 1,
		dock : 'top',
		items : [{
			xtype : 'button',
			text : 'Criar',
			itemId : 'criatipoanexo'
		}, {
			xtype : 'button',
			text : 'Modificar',
			itemId : 'modificatipoanexo'
		}, {
			xtype : 'button',
			text : 'Apagar',
			itemId : 'apagatipoanexo'
		}]
	}]
}); 