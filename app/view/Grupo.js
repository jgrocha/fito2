Ext.define('Agricultor.view.Grupo', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.grupo',
	title : 'Grupos',
	store : 'Grupos',
	columns : [{
		header : 'Id',
		dataIndex : 'id',
		width : 20
	}, {
		header : 'Nome',
		dataIndex : 'nome',
		flex : 1
	}],
	dockedItems : [{
		xtype : 'toolbar',
		flex : 1,
		dock : 'top',
		items : [{
			xtype : 'button',
			text : 'Criar',
			itemId : 'criagrupo'
		}, {
			xtype : 'button',
			text : 'Modificar',
			itemId : 'modificagrupo'
		}, {
			xtype : 'button',
			text : 'Apagar',
			itemId : 'apagagrupo'
		}]
	}]
}); 