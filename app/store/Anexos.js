Ext.define('Agricultor.store.Anexos', {
	extend : 'Ext.data.Store',
	model : 'Agricultor.model.Anexo',
	autoLoad: false, // após o render da grid dentro do formulário
	autoSync : true,
	pageSize : 35,
	proxy : {
		type : 'ajax',
		extraParams : {
			tabela : 'anexo',
			root : 'anexos'
		},
		api : {
			create : 'php/cria.php',
			read : 'php/lista.php',
			update : 'php/atualiza.php',
			destroy : 'php/remove.php'
		},
		reader : {
			type : 'json',
			root : 'anexos',
			successProperty : 'success'
		},
		writer : {
			type : 'json',
			writeAllFields : false,
			encode : true,
			root : 'anexos'
		}
	}
});