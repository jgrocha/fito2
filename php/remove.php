<?php
include ("db.php");

session_start ();
if (isset ( $_SESSION ['identificado'] ) && $_SESSION ['identificado']) {
	$idutilizador = $_SESSION ['id'];
} else {
	$idutilizador = 0;
}

$root = $_REQUEST ['root'];
$tabela = $_REQUEST ['tabela'];
$dados = $_REQUEST [$root];

$registo = json_decode ( $dados, true );
// curl --data 'root=grupos&tabela=grupo&grupos={"id":"19"}' http://localhost/git/fito/php/remove.php
$id = $registo ['id'];

$sql = "delete from " . $tabela . " where id = " . $id;
$affected = &$mdb2->exec ( $sql );
if (PEAR::isError ( $affected )) {
	$result ["success"] = false;
	$result ["errors"] ["code"] = $affected->getCode ();
	// http://pear.php.net/package/MDB2/docs/latest/__filesource/fsource_MDB2__MDB2-2.5.0b5MDB2.php.html#a694
	$result ["errors"] ["reason"] = $affected->getMessage ();
	$result ["errors"] ["query"] = $sql;
} else {
	// $linhasAfetadas = $affected;
	$result ["total"] = $affected;
	if ($affected > 0) {
		$result ["feedback"] = "O registo com o id '" . $id . "' foi removido.";
		// por o nome do utilizador no histórico criado pelo trigger, mas que não tem o idutilizador
		// não podem dois utilizadores remover a mesma linha
		// ver se há histórico para esta tabela;
		// select * from information_schema.tables where table_name= 'grupo' and table_schema = 'historico'
		// update historico.grupo     set idutilizador = 2 where idgrupo = 4
		// update historico.tipoanexo set idutilizador = 2 where idtipoanexo = 4
		$historico = 'update historico.' . $tabela . ' set idutilizador = ' . $idutilizador . ' where id' . $tabela . ' = ' . $id . ' and idutilizador IS NULL';
		$affectedUpdated = & $mdb2->exec ( $historico );
		if (PEAR::isError ( $affectedUpdated )) {
			$result ["errors"] ["reason"] = $affectedUpdated->getMessage ();
			$result ["errors"] ["query"] = $historico;
		} else {
			if ($affectedUpdated > 0) {
				$result ["historico"] = "O historico com o '" . $id . "' foi alterado com sucesso.";
			} else {
				$result ["historico"] = "O historico com o  '" . $id . "' não foi alterado.";
			}
		}		
	} else {
		$result ["feedback"] = "O registo com o id '" . $id . "' não foi removido.";
	}
	$result ["success"] = true;
	$result ["sql"] = $sql;
}
header ( 'Content-type: application/json' );
echo json_encode ( $result );
$mdb2->disconnect ();
?>