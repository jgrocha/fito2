Ext.define('Agricultor.controller.TipoAnexo', {
	extend : 'Ext.app.Controller',
	models : ['TipoAnexo'],
	stores : ['TipoAnexos'],
	requires : ['Ext.window.MessageBox'],
	// Referencing view instances
	refs : [{
		// A component query
		selector : 'viewport > app-main > tabpanel > tipoanexo',
		ref : 'tabelaTipoAnexos' // gera um getTabelaTipoAnexos
	}],
	init : function() {
		console.log('O controlador foi inicializado');
		// escutar eventos da UI
		this.control({
			// observar a vista onde são apresentados os tipoanexos
			'tipoanexo' : {
				selectionchange : this.onTipoAnexoSelect,
				render : this.render
			},
			"tipoanexo button#criatipoanexo" : {
				click : this.onButtonClickFormCriaTipoAnexo
			},
			"tipoanexo button#modificatipoanexo" : {
				click : this.onButtonClickFormModificaTipoAnexo
			},
			"tipoanexo button#apagatipoanexo" : {
				click : this.onButtonClickApagaTipoAnexo
			}
		});
		this.getTipoAnexosStore().proxy.addListener("exception", function(proxy, response, operation) {
			var resultado = Ext.JSON.decode(response.responseText);
			Ext.Msg.show({
				title : 'Erro',
				msg : resultado.errors.reason,
				icon : Ext.Msg.ERROR,
				buttons : Ext.Msg.OK
			});
			this.getTipoAnexosStore().rejectChanges();
		}, this);
	},

	onButtonClickFormCriaTipoAnexo : function(button, e, options) {
		console.log('Vamos criar um TipoAnexo');
	},

	onButtonClickFormModificaTipoAnexo : function(button, e, options) {
		console.log('Vamos modificar um TipoAnexo');
	},

	onButtonClickApagaTipoAnexo : function(button, e, options) {
		console.log('Vamos apagar um TipoAnexo');
	},

	render : function(component, options) {
		console.log('a grid tipoanexo foi renderizada.');
		// component.getStore().load();
		var store = component.getStore();
		if (store.getCount() == 0) {
			store.load();
		}
	},

	onLaunch : function() {
		console.log('evento: onLaunch, class: Agricultor.controller.TipoAnexo');
	},

	onTipoAnexoSelect : function(selModel, selection) {
		console.log('O TipoAnexo foi selecionado:');
		if (selection[0]) {
			console.debug(selection[0].data);
		}
	}
});
