<?php
include ("db.php");

session_start ();
if (isset ( $_SESSION ['identificado'] ) && $_SESSION ['identificado']) {
	$idutilizador = $_SESSION ['id'];
} else {
	$idutilizador = 0;
}

$idrequerimento = $_REQUEST ['idrequerimento'];
// $documento = $_REQUEST ['documento'];
$tipo = $_REQUEST ['tipo'];

/*
id serial PRIMARY KEY,
idrequerimento integer REFERENCES requerimento (id),
-- idtitular integer NOT NULL -- titular
documento character varying(100),
tipo integer REFERENCES tipoanexo (id),
tamanho integer,
-- http://www.verot.net/php_class_upload_download.htm
datacriacao timestamp with time zone DEFAULT now(),
datamodificacao timestamp with time zone DEFAULT now(),
-- poderá ter que haver uma tabela própria para validações
validado boolean DEFAULT NULL,
idutilizador integer REFERENCES utilizador (id), -- utilizador
datavalidacao timestamp with time zone DEFAULT NULL
*/

// há upload envolvido...
if (isset($_FILES)) {
	$file_tmp = $_FILES['documento']['tmp_name'];
	$file_name = str_replace(' ', '_', $_FILES['documento']['name']);
	$file_size = $_FILES['documento']['size'];

	$erro = $_FILES['documento']['error'];
	// $resposta["debug"]["is_uploaded_file"] = $erro;

	$pasta = "../upload/anexos/" . strval($idutilizador);
	$pastaCliente = "upload/anexos/" . strval($idutilizador);
	if (!is_dir($pasta)) {
		if (!mkdir($pasta)) {
			$resposta['success'] = false;
			$resposta["errors"]["reason"] = 'failed to create folder ' . $pasta;
			die(json_encode($resposta));
		}
	}
	
	$caminho = $pasta . "/$file_name";
	$caminhoCliente = $pastaCliente . "/$file_name";
	$resposta['caminho'] = $caminhoCliente; // $caminho
	if (is_uploaded_file($file_tmp)) {
		if (move_uploaded_file($file_tmp, $caminho)) {
			$resposta['success'] = true;				
		} else {
			$resposta['success'] = false;
			$resposta["errors"]["reason"] = 'move_uploaded_file failed';
		}
	} else {
		$resposta['success'] = false;
		$resposta["errors"]["reason"] = 'is_uploaded_file test failed';
	}
}

$tabela = 'anexo';
$chaves = 'idrequerimento, tipo, documento';
$valores = '1, ' . $tipo . ", '" . $caminhoCliente . "'";

$sql = "INSERT into " . $tabela . " (" . $chaves . ") values (" . $valores . " ) ";
$affected = &$mdb2->exec ( $sql );
if (PEAR::isError ( $affected )) {
	$result ["success"] = false;
	$result ["errors"] ["reason"] = $affected->getMessage ();
	$result ["errors"] ["query"] = $sql;
} else {
	// sacar o ID inserido
	$id = $mdb2->lastInsertID ( $tabela, 'id' );
	if (PEAR::isError ( $id )) {
		$result ["success"] = false;
		$result ["errors"] ["reason"] = $id->getMessage ();
	} else {
		$query = "select * from " . $tabela;
		$query .= " where id = " . $id;
		$resQuery = $mdb2->query ( $query );
		if (PEAR::isError ( $resQuery )) {
			$result ["success"] = false;
			$result ["errors"] ["reason"] = $resQuery->getMessage ();
			$result ["errors"] ["query"] = $query;
		} else {
			$row = $resQuery->fetchRow ( MDB2_FETCHMODE_ASSOC );
			// SUCESSO!
			// passo todo o resultado para o cliente
			$tabela = array ();
			array_push ( $tabela, $row );
			$result [$root] = $tabela;
			$result ['sql'] = $sql;
			$result ["success"] = true;
		}
	}
}
header ( 'Content-type: application/json' );
echo json_encode ( $result );
$mdb2->disconnect ();
?>