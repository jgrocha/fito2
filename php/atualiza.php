<?php
include ("db.php");

session_start ();
if (isset ( $_SESSION ['identificado'] ) && $_SESSION ['identificado']) {
	$idutilizador = $_SESSION ['id'];
} else {
	$idutilizador = 0;
}

$root = $_REQUEST ['root'];
$tabela = $_REQUEST ['tabela'];
$dados = $_REQUEST [$root];

$registo = json_decode ( $dados, true );
// curl --data 'root=grupos&tabela=grupo&grupos={"id":"20","nome":"Limpeza"}' http://localhost/git/fito/php/atualiza.php
$id = $registo ['id'];
unset ( $registo ['id'] );

if ($idutilizador == 0) {
	// Não faço ideia de quem é o utilizador (web)
	$nfields = 0;
	$update = "";
} else {
	// Tenho um utilizador (web) em sessão
	$nfields = 2;
	$update = "idutilizador = " . $idutilizador . ', datamodificacao = now()';
}

foreach ( $registo as $key => $value ) {
	$sep = ($nfields == 0) ? '' : ', ';
	switch (gettype ( $value )) {
		case "boolean" :
			$valor = $value ? "'1'" : "'0'";
			break;
		case "integer" :
		case "double" :
			$valor = $value;
			break;
		case "string" :
			$valor = "'" . $value . "'";
			break;
		case "NULL" :
			$valor = "NULL";
			break;
		default :
			$valor = "'" . $value . "'";
	}
	$update .= $sep . $key . "=" . $valor;
	$nfields += 1;
}
$sql = "update " . $tabela . " set " . $update . " where id = " . $id;
$affected = & $mdb2->exec ( $sql );
if (PEAR::isError ( $affected )) {
	$result ["success"] = false;
	$result ["errors"] ["reason"] = $affected->getMessage ();
	$result ["errors"] ["query"] = $sql;
} else {
	$linhasAfetadas = $affected;
	$result ["total"] = $linhasAfetadas;
	if ($linhasAfetadas > 0) {
		$result ["feedback"] = "O registo com o '" . $id . "' foi alterado com sucesso.";
	} else {
		$result ["feedback"] = "O registo com o  '" . $id . "' não foi alterado.";
	}
	$result ["success"] = true;
	$result ["sql"] = $sql;
}
header ( 'Content-type: application/json' );
echo json_encode ( $result );
$mdb2->disconnect ();
?>