/*
 * 
 */
Ext.define('Agricultor.controller.Menu', {
	extend : 'Ext.app.Controller',
	models : [ 'Menu' ],
	stores : [ 'Menus' ],
	views : [ 'Menu' ],
 
	// Referencing view instances
	refs : [ {
		// A component query
		selector : 'viewport > app-main > tabpanel',
		ref : 'painelPrincipal' // gera um getPainelPrincipal
	} ],
 
	init : function(application) {
		console.log('O controlador dos menus foi inicializado');
		this.control({
			"menuprincipal" : {
				itemclick : this.onClickMenu
			}
		});
		this.application.on({
			scope : this,
			loginComSucesso : this.onLogin,
			logoutComSucesso : this.onLogout
		});
	},
	onLogin : function() {
		console.log('Fecha os tabs todos e reconstroi o menu');
		this.getMenusStore().getRootNode().removeAll();
		this.getMenusStore().load();
	},
	onLogout : function() {
		console.log('Fecha os tabs todos e reconstroi o menu');
		this.getMenusStore().getRootNode().removeAll();
		this.getMenusStore().load();		
	},
	onClickMenu : function(view, record, item, index, event, options) {
		console.log('Clicou');
		if (record.get('leaf')) { // record.data.leaf
			// console.debug(record);
			var mainPanel = this.getPainelPrincipal();
			var newTab = mainPanel.items.findBy(function(tab) {
				return tab.title === record.get('titulo');
			});
			if (!newTab) {
				if (Ext.ClassManager.getNameByAlias('widget.'
						+ record.get('class')) != "") {
					newTab = mainPanel.add({
						xtype : record.get('class'),
						closable : true,
						title : record.get('titulo')
					});
				}
			}
			mainPanel.setActiveTab(newTab);
		}
	},
	onLaunch : function() {
		console.log('O controlador dos menus foi lanchado');
	}
});