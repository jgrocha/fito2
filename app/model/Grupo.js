Ext.define('Agricultor.model.Grupo', {
    extend: 'Ext.data.Model',
    
    fields: [
        { name: 'id', type: 'int' },
        { name: 'nome', type: 'auto' }

    ]
});
