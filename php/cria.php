<?php
include ("db.php");

session_start ();
if (isset ( $_SESSION ['identificado'] ) && $_SESSION ['identificado']) {
	$idutilizador = $_SESSION ['id'];
} else {
	$idutilizador = 0;
}

$root = $_REQUEST ['root'];
$tabela = $_REQUEST ['tabela'];
$dados = $_REQUEST [$root];

$registo = json_decode ( $dados, true );
// curl --data 'root=grupos&tabela=grupo&grupos={"nome":"Administrativos"}' http://localhost/git/fito/php/cria.php

if ($idutilizador == 0) {
	// Não faço ideia de quem é o utilizador (web)
	$nfields = 0;
	$chaves = "";
	$valores = "";
} else {
	// Tenho um utilizador (web) em sessão
	$nfields = 1;
	$chaves = "idutilizador";
	$valores = $idutilizador;
}

foreach ( $registo as $key => $value ) {
	$sep = ($nfields == 0) ? '' : ', ';
	if ($key != '' && $value != '') {
		$chaves .= $sep . $key;
		$valores .= $sep . "'" . $value . "'";
		$nfields += 1;
	}
}

$sql = "INSERT into " . $tabela . " (" . $chaves . ") values (" . $valores . " ) ";
$affected = &$mdb2->exec ( $sql );
if (PEAR::isError ( $affected )) {
	$result ["success"] = false;
	$result ["errors"] ["reason"] = $affected->getMessage ();
	$result ["errors"] ["query"] = $sql;
} else {
	// sacar o ID inserido
	$id = $mdb2->lastInsertID ( $tabela, 'id' );
	if (PEAR::isError ( $id )) {
		$result ["success"] = false;
		$result ["errors"] ["reason"] = $id->getMessage ();
	} else {
		$query = "select * from " . $tabela;
		$query .= " where id = " . $id;
		$resQuery = $mdb2->query ( $query );
		if (PEAR::isError ( $resQuery )) {
			$result ["success"] = false;
			$result ["errors"] ["reason"] = $resQuery->getMessage ();
			$result ["errors"] ["query"] = $query;
		} else {
			$row = $resQuery->fetchRow ( MDB2_FETCHMODE_ASSOC );
			// SUCESSO!
			// passo todo o resultado para o cliente
			$tabela = array ();
			array_push ( $tabela, $row );
			$result [$root] = $tabela;
			$result ['sql'] = $sql;
			$result ["success"] = true;
		}
	}
}
header ( 'Content-type: application/json' );
echo json_encode ( $result );
$mdb2->disconnect ();
?>