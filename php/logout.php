<?php
// recupera a sessão existente
session_start();
// destroi todas as variáveis de sessão
$_SESSION = array();
// Destroi a sessão
session_destroy();
// Devolve o resultado ao cliente
$result = array();
$result['success'] = true;
$result['msg'] = 'logout';
echo json_encode($result);
?>