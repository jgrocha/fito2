-- SET client_encoding = 'UTF8';

DROP TABLE IF EXISTS grupo CASCADE;
DROP TABLE IF EXISTS menu CASCADE;
DROP TABLE IF EXISTS permissao CASCADE;
DROP TABLE IF EXISTS utilizador CASCADE;

CREATE TABLE grupo (
    id serial PRIMARY KEY,
    nome character varying(45) NOT NULL,
    -- para auditoria
    datacriacao timestamp with time zone DEFAULT now(),
    datamodificacao timestamp with time zone    
    -- idutilizador integer REFERENCES utilizador (id) -- utilizador
	-- tem que ser acrescentado depois de se criar a tabela utilizador
);

-- CREATE TABLE utilizador (
--    id serial PRIMARY KEY,
--    nome character varying(100) NOT NULL,
--    login character varying(20) NOT NULL UNIQUE,
--    password character varying(100) NOT NULL,
--    email character varying(100) NOT NULL,
--    fotografia character varying(100),
--    idgrupo integer REFERENCES grupo (id),
--    datacriacao timestamp with time zone DEFAULT now(),
--    datamodificacao timestamp with time zone,    
--    ultimologin timestamp with time zone,
--    ativo boolean DEFAULT TRUE 
-- );

CREATE TABLE utilizador (
    id serial PRIMARY KEY,
    login character varying(20) NULL UNIQUE,
    password character varying(100) NULL,
    idgrupo integer REFERENCES grupo (id),
    email character varying(100) NULL,
    fotografia character varying(100),
    nome character varying(120) NOT NULL,
    morada character varying(80) NULL,
    localidade character varying(80) NULL,
    codpostal character varying(8) NULL,
    despostal character varying(80) NULL,
    nif character varying(9) NULL, -- 
    nic character varying(9) NULL, -- Número de Identificação Civil
	-- http://www.cartaodecidadao.pt/index.php%3Foption=com_content&task=view&id=156&Itemid=35&lang=pt.html
    masculino boolean,
	pessoacoletiva boolean,
    telemovel character varying(15), -- para receber SMS
	telefone character varying(15),
    observacoes text,
    dicofre character varying(6),
    ponto geometry(Point,3763), -- coordenadas da casa
    datacriacao timestamp with time zone DEFAULT now(),
    datamodificacao timestamp with time zone DEFAULT now(),
	ultimologin timestamp with time zone,
	preferencias hstore,
	-- http://postgresguide.com/sexy/hstore.html    
    ativo boolean DEFAULT TRUE 
);

ALTER TABLE utilizador
  ADD CONSTRAINT enforce_srid_geometria CHECK (st_srid(ponto) = 3763);
ALTER TABLE utilizador
  ADD CONSTRAINT enforce_geotype_geometria CHECK (geometrytype(ponto) = 'POINT'::text OR ponto IS NULL);
ALTER TABLE utilizador
  ADD CONSTRAINT enforce_dims_geometria CHECK (st_ndims(ponto) = 2);

alter table grupo add column idutilizador integer REFERENCES utilizador (id);

CREATE TABLE menu (
    id serial PRIMARY KEY,
    titulo character varying(45) NOT NULL,
    icon character varying(15),
    idsuperior integer,
    class character varying(45),
    anonimo boolean DEFAULT FALSE
);

CREATE TABLE permissao (
    idmenu integer REFERENCES menu (id),
    idgrupo integer REFERENCES grupo (id)
);

INSERT INTO grupo (nome) VALUES ('Administradores');
INSERT INTO grupo (nome) VALUES ('Técnicos');
INSERT INTO grupo (nome) VALUES ('Aplicadores');
INSERT INTO grupo (nome) VALUES ('Vendedores');
INSERT INTO grupo (nome) VALUES ('Público');

INSERT INTO menu (titulo, idsuperior, anonimo) VALUES ('Legislação', NULL, TRUE);
INSERT INTO menu (titulo, idsuperior, anonimo) VALUES ('Aplicação', 1, TRUE);
INSERT INTO menu (titulo, idsuperior, anonimo) VALUES ('Comercialização', 1, TRUE);
INSERT INTO menu (titulo, idsuperior) VALUES ('Gestão de utilizadores', NULL);
INSERT INTO menu (titulo, idsuperior) VALUES ('Grupos', 4);
INSERT INTO menu (titulo, idsuperior) VALUES ('Utilizadores', 4);
INSERT INTO menu (titulo, idsuperior) VALUES ('Permissões', 4);
INSERT INTO menu (titulo, idsuperior) VALUES ('Gestão de tabelas', NULL);
INSERT INTO menu (titulo, idsuperior) VALUES ('Tipos de anexos', 8);
INSERT INTO menu (titulo, idsuperior) VALUES ('Códigos postais', 8);
INSERT INTO menu (titulo, idsuperior) VALUES ('Cartões', NULL);
INSERT INTO menu (titulo, idsuperior) VALUES ('Requerimentos', 11);
INSERT INTO menu (titulo, idsuperior) VALUES ('Consulta', 11);
INSERT INTO menu (titulo, idsuperior) VALUES ('Pendentes', 11);
INSERT INTO menu (titulo, idsuperior) VALUES ('Relatórios', NULL); --15
INSERT INTO menu (titulo, idsuperior) VALUES ('Por delegação', 15);
INSERT INTO menu (titulo, idsuperior) VALUES ('Por utilizador', 15);
INSERT INTO menu (titulo, idsuperior) VALUES ('Tempos de emissão', 15);
INSERT INTO menu (titulo, idsuperior, anonimo) VALUES ('Informações', NULL, TRUE); -- 19
INSERT INTO menu (titulo, idsuperior, anonimo) VALUES ('Cursos e workshops', 19, TRUE);
INSERT INTO menu (titulo, idsuperior, anonimo) VALUES ('Recortes de Imprensa', 19, TRUE);

-- admins
INSERT INTO permissao (idmenu, idgrupo) SELECT m.id, 1 FROM menu m;
-- aplicadores
INSERT INTO permissao (idmenu, idgrupo) SELECT id, 3 FROM menu m WHERE id = 1 OR idsuperior = 1 OR id = 11 OR idsuperior = 11;

-- nome, login, password, email, idgrupo    
-- CryptoJS.SHA1('123456').toString()
-- INSERT INTO titular (nome, login, password, email, idgrupo) 
-- VALUES ('Sara Rocha', 'sara', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'sarafrocha@gmail.com', 3);
-- CryptoJS.SHA1('user').toString()
-- INSERT INTO titular (nome, login, password, email, idgrupo) 
-- VALUES ('Gustavo Rocha', 'gustavo', '12dea96fec20593566ab75692c9949596833adc9', 'jgr@jorge-gustavo-rocha.pt', 1);
--
-- INSERT INTO titular (nome, login, password, email, idgrupo) 
-- VALUES ('Jorge Rocha', 'jgr', '12dea96fec20593566ab75692c9949596833adc9', 'jgr@di.uminho.pt', 1);

INSERT into utilizador (
    nome, morada, localidade, codpostal, despostal,
    nif, nic, masculino,
    telemovel, telefone,
    observacoes, dicofre, ponto, login, password, email, idgrupo)
VALUES (
'Jorge Gustavo Pereira Bastos Rocha', 'Rua Pascoal Fernandes, 11, 6 ESQ', 'Lamaçães', '4715-281', 'Braga',
'196628865', '8432271', TRUE,
'910333888', '253253586',
'Titular criado para teste', '010101', ST_GeomFromText('POINT(-22518 208748)',3763), 'gustavo', '12dea96fec20593566ab75692c9949596833adc9', 'jgr@jorge-gustavo-rocha.pt', 1
);

INSERT into utilizador (
    nome, morada, localidade, codpostal, despostal,
    nif, nic, masculino,
    telemovel, telefone,
    observacoes, dicofre, ponto, login, password, email, idgrupo)
VALUES (
'Joaquim Ferreira da Rocha', 'Rua Gil Vicente', 'Nogueira', '4715-193', 'Braga',
'510906109', '12345678', TRUE,
'910333131', '253687290',
'Titular criado para teste', '010101', ST_GeomFromText('POINT(-22400 208885)',3763), 'quim', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'sarafrocha@gmail.com', 3
);

update menu set class = 'grupo' where titulo = 'Grupos';
update menu set class = 'tipoanexo' where titulo = 'Tipos de anexos';
update menu set class = 'requerimento' where titulo = 'Requerimentos';

update menu set class = 'maquina' where titulo = 'Máquinas';

-- select * from grupo;
-- update grupo set nome = 'Aplicadores de porcarias', idutilizador = 1 where id = 3;
-- delete from grupo where id = 4;
-- select * from historico.grupo;
